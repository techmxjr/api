console.log("Aqui funcionando con nodemon");

var movimientosJSON = require('./movimientosv2.json');
var usuariosJSON = require('./usuariosv1.json');
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require ('request-json');
var isodate = require("isodate");

var app = express();
app.use(bodyParser.json())

app.get('/',function(req,res) {
    res.send('Hola mundo-API');
});

app.get('/v1/movimientos',function(req,res) {
    //sendfile is deprecated
    res.sendfile('movimientosv1.json');
});

app.get('/v2/movimientos',function(req,res) {
    //sendfile is deprecated
    //res.sendfile('movimientosv2.json');
    res.send(movimientosJSON);
});

app.get('/v2/movimientosq',function(req,res) {
    console.log(req.query);
    res.send('Recibido');
});

app.get('/v2/movimientos/:id',function(req,res) {
    res.send(movimientosJSON[req.params.id-1]);
});

app.post('/v2/movimientos',function(req,res) {
    console.log(req);
    if(req.headers['authorization']!=undefined){
      var nuevo=req.body;
      nuevo.id = movimientosJSON.length+1;
      movimientosJSON.push(nuevo);
      res.send('Requerimiento dado de alta');
    }else{
      res.send('No esta autorizado');
    }
});

app.put('/v2/movimientos/:id',function(req,res) {
    var actual=movimientosJSON[req.params.id-1];
    var modificado=req.body;
    if(modificado.importe != undefined){
      actual.importe=modificado.importe;
    }
    if(modificado.ciudad != undefined){
      actual.ciudad=modificado.ciudad;
    }
    res.send('Requerimiento Modificado');
});

app.delete('/v2/movimientos/:id',function(req,res){
    var actual=movimientosJSON[req.params.id-1];
    movimientosJSON.push({
      "id": movimientosJSON.length+1,
      "ciudad": actual.ciudad,
      "importe": actual.importe * (-1),
      "concepto": "negativo del " + req.params.id
    });
    res.send('Movimiento anulado');
});

app.get('/v2/usuarios',function(req,res) {
    res.send(usuariosJSON);
});

app.get('/v2/usuarios/:id',function(req,res) {
    res.send(usuariosJSON[req.params.id-1]);
});

app.post('/v2/usuarios/login',function(req,res) {
    console.log(req.headers);
    //var consulta = jsonQuery('[email='+req.headers.usuario+']',{data:usuariosJSON})
    for(i=0;i<usuariosJSON.length;i++){
      var usr = usuariosJSON[i];
      if(usr.email==req.headers.usuario && usr.pass==req.headers.pass){
        usr.estado="Autenticado";
        res.send('Usuario Autenticado');
        break;
      }
    }
    res.send('Usuario/Pass incorrecto');
});

app.post('/v2/usuarios/logout',function(req,res) {
    console.log(req.headers);
    for(i=0;i<usuariosJSON.length;i++){
      var usr = usuariosJSON[i];
      if(usr.id==req.headers.idusr){
        usr.estado="null";
        res.send('Usuario Desconectado');
        break;
      }
    }
    res.send('El usuario no estaba conectado');
});

//version 3 de la API conectada a mlab
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techujlcmmx/collections";
var apiKey = "apiKey=XdS26VYeiXpzps4RVXXqsT2MMQrCiMV3";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);

app.get('/v3', function(req, res){
  clienteMlab.get('', function(err, resM, body){
    var coleccionesUsuario = [];
    if (!err) {
      for (var i = 0; i < body.length; i++){
        if(body[i] != "system.indexes"){
          coleccionesUsuario.push({"recurso":body[i],"url":"/v3/"+body[i]})
        }
      }
      res.send(coleccionesUsuario);
    }else {
      res.send(err);
    }

  });
});

app.get('/v3/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/usuarios/:id',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    clienteMlab.get('?q={"idusuario":"'+req.params.id+'"}&'+apiKey, function(err, resM, body){
      res.send(body);
    });
});

app.post('/v3/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.post('',req.body, function(err, resM, body){
      res.send(body);
    });
});

app.put('/v3/usuarios/:id',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    var cambio= '{"$set":'+JSON.stringify(req.body)+'}';
    //res.send('?q={"$set":'+req.body+'}&'+apiKey);
    clienteMlab.put('?q={"id":'+req.params.id+'}&'+apiKey,JSON.parse(cambio), function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/movimientos',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/movimientos?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v3/movimientos/:idcuenta',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/movimientos");
    clienteMlab.get('?'+apiKey+'&q={"idcuenta":"'+req.params.idcuenta+'"}', function(err, resM, body){
      res.send(body);
    });
});

//version 4 de la API
app.get('/v4',function(req,res) {
    //sendfile is deprecated
    res.sendfile('dummy.pdf');
});


/*
var urlMlabRaiz="https://api.mlab.com/api/1/databases/techujlcmmx/collections";
var apiKey = "apiKey=XdS26VYeiXpzps4RVXXqsT2MMQrCiMV3";
var clienteMlab=requestJson.createClient(urlMlabRaiz+"?"+apiKey);
*/
//Versiòn 5: Manejo de cuentas
app.get('/v5/cuentas/:idusuario',function(req,res) {
  //  console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"idusuario":"'+req.params.idusuario+'"}');
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");
    clienteMlab.get('?'+apiKey+'&q={"idusuario":"'+req.params.idusuario+'"}&f={"idcuenta":1,"saldo":1}', function(err, resM, body){
      res.send(body);
    });
});

app.get('/v5/cuentas',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas?"+apiKey);
    clienteMlab.get('', function(err, resM, body){
      res.send(body);
    });
});

//Consulta de movimientos en la colección Cuentas por ID Cuenta
app.get('/v5/movimientos/:idusuario&:idcuenta',function(req,res) {
  if(!req.params.idusuario || !req.params.idcuenta)
  {
    //console.log(req.params.idusuario)
    //console.log(req.params.idcuenta)
    res.send('Consulta Mov por IDUSR y Cuenta: Usuario y Cuenta requeridos');
  }else {
    var idusuario= req.params.idusuario;

    //console.log(req.params.idusuario)
    //console.log(req.params.idcuenta)

    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");
//  console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1}&s={"fecha":1}')
                                clienteMlab.get('?'+apiKey+'&q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1,"saldo":1}&s={"fecha":1}', function(err, resM, body){

    //console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1}&s={"fecha":1}');

    //clienteMlab.get('?'+apiKey+'&q={"idcuenta":'+req.params.idcuenta+'}&f={"movimientos":1}&s={"fecha":1}', function(err, resM, body){
    //console.log('Movimientos en el body de respuesta:' +'' +JSON.stringify(body[0].movimientos.length))
      res.send(body);

    });
    }
});

// Alta de Usuarios en la colección Usuarios
app.post('/v5/usuarios',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios?"+apiKey);
    clienteMlab.post('',req.body, function(err, resM, body){
      res.send(body);
      //console.log(body)
    });
});

//Consulta de usuarios enviando email y password en el body
app.post('/v5/usuarios/login',function(req,res) {
  if(!req.body.email || !req.body.password)
  {
    res.send('Error en login, Usuario y password no pueden ir vacíos');
  }else {
       var email= JSON.stringify(req.body.email);
       var password= JSON.stringify(req.body.password);
       clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    //console.log(urlMlabRaiz+"/usuarios"+'?'+apiKey+'&q={"email":"'+JSON.parse(email)+'","password":"'+JSON.parse(password)+'"}&f={idusuario:1,nombre:1,apellido:1,email:1,pais:1,_id:0}');
                        clienteMlab.get('?'+apiKey+'&q={"email":"'+JSON.parse(email)+'","password":"'+JSON.parse(password)+'"}&f={idusuario:1,nombre:1,apellido:1,email:1,pais:1,_id:0}', function(err, resM, body){
                          //console.log(JSON.stringify(body.length));
                          //console.log(JSON.stringify(body));

                          if (JSON.stringify(body.length) !=1) {
                            console.log('Login: No existe el usuario');
                            //res.send(body);
                            res.json({success: false, message: 'No existe el usuario'});
                            //console.log(res.body)
                          } else {
                            //console.log(JSON.stringify(body[0]));
                            res.send(body);
                            }
            });
  }
});

//Alta de movimientos en la colección Cuentas por Id Usuario y ID Cuenta
/*app.put('/v5/movimientos/:idusuario&:idcuenta',function(req,res){
  if(!req.params.idusuario || !req.params.idcuenta)
  {
    console.log(req.params.idusuario)
    console.log(req.params.idcuenta)
    res.send('Error en Alta de Movimiento, Usuario y Cuenta requeridos');
  }else {
    var idusuario= req.params.idusuario;
    var icuenta= req.params.idcuenta;

    console.log(req.params.idusuario)
    console.log(req.params.idcuenta)
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");

    console.log(urlMlabRaiz+"/usuarios"+'?'+apiKey+'&q={"idusuario":"'+JSON.parse(idusuario)+'","idcuenta":"'+JSON.parse(idcuenta)+'"}&f={idusuario:1,nombre:1,apellido:1,email:1,pais:1,_id:0}');
                        clienteMlab.put('?'+apiKey+'&q={"idusuario":"'+JSON.parse(idusuario)+'","idcuenta":"'+JSON.parse(idcuenta)+'"}&f={"movimientos":1}&s={"fecha":1}', function(err, resM, body){
      res.send(body);
      console.log(res.body)

        //response.errors[0].messages.length.should.equal(2);
    });

  }
});*/
//Alta de movimientos en cuentas realizando consulta previa
app.put('/v5/movimientos/:idusuario&:idcuenta',function(req,res){
  if(!req.params.idusuario || !req.params.idcuenta)
  {
    //console.log(req.params.idusuario)
    //console.log(req.params.idcuenta)
    res.send('Alta de Mov: Error en Alta de Movimiento, Usuario y Cuenta requeridos');
    res.json({success: false, message: 'Error en alta de Movimientos: Ingrese Usuario y Cuenta válida'});
  }else {
    var idusuario= req.params.idusuario;
    //console.log(req.params.idusuario)
    //console.log(req.params.idcuenta)

    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas");
    //console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'&q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&f={movimientos:1,_id:0}&s={"fecha":1}')
                                  clienteMlab.get('?'+apiKey+'&q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&f={movimientos:1,_id:0}&s={"fecha":1}', function(err, resM, body){
                                   //console.log (body.length)
                                    //console.log(body[0].movimientos.idmov)
                                    //console.log('Contenido del body[0].legth: ' +' ' +body[0].length)
                                    //console.log('Contenido del req.body: ' +' ' +req.body)
                                    //console.log('Contenido del JSON.stringifybody[0]: ' +' '+JSON.stringify(body[0]))

                                    if (JSON.stringify(body[0])== '{}') {
                                  //  if (JSON.stringify(body[0])== undefined) {
                                      //console.log('Vengo vacio');
                                      /*var alta mov = []
                                      altamov.push({
                                        "movimientos":[{"idmov":1,
                                      }]
                                      }

                                    )*/
                                    //console.log('Contenido del body[0].legth: ' +' ' +body[0].length)
                                    //console.log('Contenido del body[0].legth: ' +' ' +req.body.movimientos)
                                    var altaMov = []
                                    //var date= new Date();
                                    altaMov.push({
                                        "idmov": 1,
                                        "fecha":{"$date":new Date()},
                                        "descripcion": req.body.movimientos[0].descripcion,
                                        "importe": req.body.movimientos[0].importe,
                                        "latitud" : req.body.movimientos[0].latitud,
                                        "longitud" : req.body.movimientos[0].longitud
                                      })
                                      //var cambio= '{"$set":'+JSON.stringify(req.body)+'}';
                                    var cambio= '{"$set":'+'{"saldo":'+req.body.saldo+',"movimientos":'+JSON.stringify(altaMov)+'}}';
                                    //  var cambio= '{"$set":'+'{"movimientos":'+JSON.stringify(actualesMov)+'}}';
                                      //console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'?q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&'+apiKey,JSON.parse(cambio));
                                      clienteMlab.put('?q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&'+apiKey,JSON.parse(cambio), function(err, resM, body){
                                        res.send (body);
                                      });

                                    } else {
                                      //console.log('No Vengo vacio');
                                      var nuevoMov = []
                                      var actualesMov = []
                                      actualesMov =  body[0].movimientos
                                      nuevoMov = req.body.movimientos
                                      //nuevoMov = JSON.stringify(foo,req.body.movimientos);
                                      //console.log('Movimientos en el body de respuesta:' +'' +JSON.stringify(body[0].movimientos))
                                      //console.log('Movimientos en el body request:' +'' +JSON.stringify(req.body.movimientos))
                                      //actualesMov.push(req.body.movimientos[1])
                                      //console.log('Valor del array actualesmov:' +'' +JSON.stringify(actualesMov));
                                      //console.log('Valor del array actualesmov:' +'' +JSON.stringify(actualesMov.idmov));
                                      //console.log('Valor del array NuevoMov:' +'' +JSON.stringify(nuevoMov));
                                      //console.log('Valor del array NuevoMov:' +'' +nuevoMov);
                                      //console.log('Valor del array NuevoMov:' +'' +nuevoMov[1]);
                                      //console.log('Movimientos en el body de respuesta:' +'' +JSON.stringify(body[0].movimientos.length))
                                      //console.log('Movimientos en nuevo Array ActualesMov:' +'' +JSON.stringify(actualesMov.length))

                                      for (var i = 0; i < body[0].movimientos.length; i++) {
                                        actualesMov[i] = body[0].movimientos[i];
                                        //console.log('Entre al for, valor del idmovimiento' + JSON.stringify(actualesMov));
                                        break;
                                      }

                                      //console.log('Valor del array actualesmov[0].idmov:' +'' +JSON.stringify(nuevoMov[0].idmov));
                                      //console.log('Fecha Actual:' +  Date.now().toISOString());
                                      //var d= new Date().toISOString();
                                      var date= new Date();
                                      //console.log('Fecha Actual:' +d);
                                      var idMov = body[0].movimientos.length
                                      actualesMov.push({
                                          "idmov": idMov+1,
                                          //"fecha":nuevoMov[0].fecha,
                                          "fecha":{"$date":date},
                                          "descripcion": nuevoMov[0].descripcion,
                                          "importe": nuevoMov[0].importe,
                                          "latitud" : nuevoMov[0].latitud,
                                          "longitud" : nuevoMov[0].longitud
                                        })

                                    //console.log(JSON.stringify(actualesMov));
                                    //console.log('Movimientos en el body de respuesta:' +'' +JSON.stringify(actualesMov.length))

                                    var cambio= '{"$set":'+'{"saldo":'+req.body.saldo+',"movimientos":'+JSON.stringify(actualesMov)+'}}';
                                    //var cambio= '{"$set":'+'{"movimientos":'+JSON.stringify(actualesMov)+'}}';
                                  //console.log('Valor cambio' + ' ' +cambio)
                                  //  var cambio= '{"$set":'+JSON.stringify(req.body)+'}';
                                    //console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey+'?q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&'+apiKey,JSON.parse(cambio));
                                                                  clienteMlab.put('?q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&'+apiKey,JSON.parse(cambio), function(err, resM, body){
                                  //clienteMlab.get('?'+apiKey+'&q={"idusuario":"'+idusuario+'","idcuenta":'+req.params.idcuenta+'}&f={movimientos:1,_id:0}&s={"fecha":1}', function(err, resM, body){
                                    res.send(body);
                                  });

                                  }

    });
  }
});


//Alta de Cuentas
app.post('/v5/cuentas',function(req,res) {
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/cuentas?"+apiKey);
    //console.log(req.body)
    //console.log(urlMlabRaiz+"/cuentas"+'?'+apiKey);
    clienteMlab.post('',req.body, function(err, resM, body){
      res.send(body);
      //console.log(body)
    });
});

//Actualizaciòn de datos de usuarios
app.put('/v5/usuarios/:idusuario',function(req,res) {
  if(!req.params.idusuario)
  {
    //console.log(req.params.idusuario)
    res.send('Actualización de Usuario: Error en actualización de usuario, Usuario requerido');
    res.json({success: false, message: 'Actualización de Usuario: Error en actualización de usuario, Usuario requerido'});

  }else {
    var idusuario= req.params.idusuario;
    //console.log(req.params.idusuario)
    clienteMlab = requestJson.createClient(urlMlabRaiz+"/usuarios");
    var cambio= '{"$set":'+JSON.stringify(req.body)+'}';
    clienteMlab.put('?q={"idusuario":"'+idusuario+'"}&'+apiKey,JSON.parse(cambio), function(err, resM, body){
      res.send(body);
      //console.log(body)
    });
  }
});

app.listen(3000);
console.log("Escuchando en el puerto 3000");
