var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var server = require('../server')
var isodate = require("isodate");

chai.use(chaiHttp); //configurar chai con el modulo http

//declarar conjunto de pruebas (test-suites)
//utilizar el modo breve
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.com.mx').get('/').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        done();
      })
  })
});

describe('Tests de api-jluisRolando', () => {
  it('Raiz de la API contesta', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        done();
      })
  })
  it('Raiz de la API funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res);
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      })
  })
  it('Raiz de la API devuelve 2 colecciones', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        //res.body.length.should.be.eql(2);
        done();
      })
  })
  it('Raiz de la API devuelve los objetos correctos', (done) => {
    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
        //console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        /*res.body.length.should.be.eql(2);
        for (var i = 0; i < res.body.length; i++) {
          res.body[i].should.have.property('recurso');
          res.body[i].should.have.property('url');
        }*/
        done();
      })
  })
});

/*describe('Tests de api movimientos', () => {
  it('Raiz de la API movimientos contesta', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
        res.should.have.status(200);
        done();
      })
  })
  it('Raiz de la API movimientos funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos').end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('array');
        done();
      })
  })
  it('Consulta movimiento concreto funciona', (done) => {
    chai.request('http://localhost:3000').get('/v3/movimientos/mx0001').end((err, res) => {
        console.log(res.body);
        res.should.have.status(200);
        res.body.should.be.a('array');
        res.body[0].should.have.property('idcuenta');
        done();
      })
  })
});*/

describe('API Version 5...Test de API Cuentas, Usuarios y Movimientos a MLab', () => {
  it('API MLab responde y envia cadena de conexión', (done) => {
    chai.request('https://api.mlab.com/api/1/databases/techujlcmmx/?apiKey=XdS26VYeiXpzps4RVXXqsT2MMQrCiMV3').get('').end((err,res) => {
      res.should.have.status(200);
      done()
    })
  })
  it('Consulta de usuario por email y password funciona', (done) => {
    chai.request('http://localhost:3000').post('/v5/usuarios/login').send({
      "email":"tjosilevich5@unesco.org",
      "password":"DldZOmHzt"}).end((err,res) => {
      //console.log(res);
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('array');
       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');

       //res.body.should.have.property('nombre');
       //res.body.should.have.property('apellido');
       //res.body.should.have.property('email');
       //res.body.should.have.property('pais');

      done()
    })
  })

  it('Alta de usuario funciona', (done) => {
    chai.request('http://localhost:3000').post('/v5/usuarios/').send({
      "idusuario":"USR88888",
      "nombre":"TestChai",
      "apellido":"Apellido Chai",
      "email":"chai@chai.com.mx",
      "password":"Chai",
      "pais":"México"
      }).end((err,res) => {
      //console.log(res);
      res.should.have.status(200);
      res.should.be.json;
      //res.body.should.be.a('array');
       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
      res.body.should.have.property('idusuario');
       res.body.should.have.property('nombre');
       res.body.should.have.property('apellido');
       res.body.should.have.property('email');
       res.body.should.have.property('password');
       res.body.should.have.property('pais');
       res.body.should.have.property('_id');

      done()
    })
  })
  it('Alta de cuentas funciona', (done) => {
    chai.request('http://localhost:3000').post('/v5/cuentas/').send({
      "idusuario":"USR88888",
      "idcuenta":11,
      "saldo":99999.99
      }).end((err,res) => {
      //console.log(res);
      res.should.have.status(200);
      res.should.be.json;
      //res.body.should.be.a('array');
       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
       res.body.should.have.property('idusuario');
       res.body.should.have.property('idcuenta');
       res.body.should.have.property('saldo');
       res.body.should.have.property('_id');

      done()
    })
  })
  it('Modificación de usuario funciona', (done) => {
    chai.request('http://localhost:3000').put('/v5/usuarios/USR88888').send({
      "email":"chai1@chai1.com.mx",
      "pais":"México Modificado",
      "password":"Chai1"
      }).end((err,res) => {
      //console.log(res);
      res.should.have.status(200);
      res.should.be.json;
      //res.body.should.be.a('array');
       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
      res.body.should.have.property('n').to.be.equals(1);
      //expect(res).to.have.status(200);
      done()
    })
  })
  it('Consulta de cuentas por usuario funciona', (done) => {
    chai.request('http://localhost:3000').get('/v5/cuentas/USR88888').end((err,res) => {
      //console.log(res);
      //expect(req.params).to.be.equals('idusuario')
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('array');

       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
      //expect(res).to.have.status(200);
      //res.body.should.have.property('idcuenta');
      //sres.body.should.have.property('saldo');

      done()
    })
  })
  it('Alta de Movimientos por usuario y cuentas funciona', (done) => {
    chai.request('http://localhost:3000').put('/v5/movimientos/USR88888&11').send(
      {
        "saldo": 99999.99,
       "movimientos" : [ { "idmov" : 1,
                "fecha" : { "$date" : "2018-02-23T08:27:58.000Z"},
                "descripcion" : "Pago de Rolitas",
                "importe" : 520.64,
                "latitud" : 52.868426,
                "longitud" : 520.320328}]

    }
  ).end((err,res) => {
    //  console.log(res);
      //expect(req.params).to.be.equals('idusuario')
      res.should.have.status(200);
      res.should.be.json;
      //res.body.should.be.a('array');
      res.body.should.have.property('n').to.be.equals(1);

       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
      //expect(res).to.have.status(200);
      //res.body.should.have.property('idcuenta');
      //res.body.should.have.property('saldo');

      done()
    })
  })
  it('Consulta de Movimientos por usuario y cuentas funciona', (done) => {
    chai.request('http://localhost:3000').get('/v5/movimientos/USR88888&11').end((err,res) => {
      //console.log(res);
      //expect(req.params).to.be.equals('idusuario')
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('array');

       //res.ARRAY.should.have.property('idusuario')
       //NOres.body.should.be.a('object')
       //res.body.[{}].should.have.property('idusuario')
       //array.every(i => expect(i).to.have.all.keys)
       //res.body.ARRAY.should.be.a('object')
       //res.body.object.should.have.property('idusuario');
       //res.body.ARRAY.should.have.property('idusuario');
       //res.body.ARRAY.should.be.a('object');
       //res.body.ARRAY.should.have.be.a('object');
       //res.body.should.have.property('idusuario');
      //expect(res).to.have.status(200);
      //res.body.should.have.property('idcuenta');
      //res.body.should.have.property('saldo');

      done()
    })
  })
});
